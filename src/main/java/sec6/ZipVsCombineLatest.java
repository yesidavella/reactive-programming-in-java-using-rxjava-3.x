package sec6;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;

public class ZipVsCombineLatest {

	public static void main(String[] args) throws InterruptedException {
		
		
		Observable<String> obsA = Observable.interval(250, TimeUnit.MILLISECONDS)
				.take(30)
				.map(e->"obsA"+e);
		Observable<String> obsB = Observable.interval(500, TimeUnit.MILLISECONDS)
				.take(15)
				.map(e->"obsB"+e);
		Observable<String> obsC = Observable.interval(50, TimeUnit.MILLISECONDS)
				.take(150)
				.map(e->"obsC"+e);
		
		System.out.println("---------------Using zip()----------------------");
		@NonNull
		Disposable disposable1 = Observable.zip(obsA, obsB, obsC, (e1,e2,e3)->"zipped:"+e1+"+"+e2+"+"+e3)
		.subscribe(r->System.out.println("Final result of zip(): "+r));
		
		Thread.sleep(28500);
		disposable1.dispose();
		
		System.out.println("---------------Using combineLatest()----------------------");
		Observable.combineLatest(obsA, obsB, obsC, (e1,e2,e3)->"combined:"+e1+"+"+e2+"+"+e3)
		.subscribe(r->System.out.println("Final result of combineLatest(): "+r));
		
		Thread.sleep(28500);
	}
}
