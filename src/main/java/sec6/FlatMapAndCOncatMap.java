package sec6;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observable;

public class FlatMapAndCOncatMap {

	public static void main(String[] args) {

		System.out.println("+++++++++++++Using FlatMap()+++++++++++++");
		List<String> list = List.of("Hola", "Amigos", "Como", "Estan");
		Observable.fromIterable(list)
		//.map(e -> Observable.fromArray(e.split("")))
		.flatMap(e -> Observable.fromArray(e.split("")))
		.subscribe(x -> System.out.println(x));
		
		System.out.println("+++++++++++++Using concatMap()+++++++++++++");
		List<String> list2 = List.of("Cha", "Chicos", "Hasta", "Luego");
		Observable.fromIterable(list2)
		.concatMap(e -> Observable.fromArray(e.split("")))
		.subscribe(x -> System.out.println(x));

	}

}
