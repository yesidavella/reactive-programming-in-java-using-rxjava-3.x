package sec6;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;

public class MergeAndConcat {

	public static void main(String[] args) throws InterruptedException {
		
		Observable obs1 = Observable.just("hola", "amigos","todo","bien?");
		Observable obs2 = Observable.just("me","voy","amigos");
		
		System.out.println("+++++++++++Ejemplo merge() in sequence execution++++++++++++");
		Observable.merge(obs1,obs2).subscribe(r -> System.out.println("Sequential merge() printing: "+r));
		
		
		Observable obs3 = Observable.interval(1, TimeUnit.SECONDS).map(e -> e+"Obs3");
		Observable obs4 = Observable.interval(1, TimeUnit.SECONDS).map(e -> e+"Obs4");
		
		System.out.println("+++++++++++Ejemplo merge() in parallel++++++++++++");
		@NonNull
		Disposable dispose = Observable.merge(obs3,obs4).subscribe(r -> System.out.println("Parallel merge() printing: "+r));
		Thread.sleep(10000);
		dispose.dispose();
		

		System.out.println("+++++++++++Ejemplo concat()++++++++++++");
		Observable.concat(Observable.just("fist","second","thrid"),Observable.just("primero","segundo","tercero"))
		.subscribe(r -> System.out.println("concat() printing: "+r));
		
		Thread.sleep(10000);
		
		
	}

}
