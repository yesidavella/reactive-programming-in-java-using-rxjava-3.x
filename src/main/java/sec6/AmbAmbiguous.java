package sec6;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Observable;

public class AmbAmbiguous {

	public static void main(String[] args) throws InterruptedException {
		
		
		Observable<String> obs1 = Observable.interval(300, TimeUnit.MILLISECONDS)
				.take(50).map(e ->"Obs1="+e);
		
		Observable<String> obs2 = Observable.interval(300, TimeUnit.MILLISECONDS)
				.take(50).map(e ->"Obs2="+e);
		
		Observable<String> obs3 = Observable.interval(301, TimeUnit.MILLISECONDS)
				.take(50).map(e ->"Obs3="+e);
		
		/**
		 * In general ams(), ambArray() and its variants, takes the observable who is
		 * emitting faster.
		 */
		Observable.ambArray(obs3,obs2,obs1)
		.subscribe(e->System.out.println(e));
		
		Thread.sleep(30000);
		System.out.println("Fin de ejecucion...");

	}

}
