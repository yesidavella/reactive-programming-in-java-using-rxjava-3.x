package sec1;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Consumer;

public class HelloRxJava {

	public static void main(String[] args) {

		Observable<String> source = Observable.create(

				e -> {
					e.onNext("Hola");
					e.onNext("rxJava");
				});

		
		Consumer c = v -> System.out.println(Thread.currentThread().getName()+v);
		source.subscribe(e -> System.out.println(Thread.currentThread().getName()+". Consumer 1: " + e));
		source.subscribe(e -> System.out.println(Thread.currentThread().getName()+". Consumer 2: " + e));
		source.subscribe(c);

	}

}
