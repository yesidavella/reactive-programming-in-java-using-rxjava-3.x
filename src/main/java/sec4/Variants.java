package sec4;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class Variants {

	public static void main(String[] args) {

		Observable<String> observable = Observable.just("Yesid", "Andres", "Avella");

		Single.just("jaja");
		Maybe.just("depronto").subscribe(e -> System.out.println(e));

		Runnable r = new Runnable() {

			@Override
			public void run() {
				System.out.println("I am starting to run....");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("I am finishing to run....");
			}
		};

		Completable		
		//.fromRunnable(()->System.out.println("blablablabla..."))
		//.fromRunnable(r)
		.fromRunnable(()-> concurrentMethod())
		.subscribe(() -> System.out.println("termine!!! ejecutando onCOmplete() en CompletableObserver"));

	}

	public static void concurrentMethod() {
		System.out.println("I am starting to run.... wait 10 secs...");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("I am finishing to run....");
	}
}
