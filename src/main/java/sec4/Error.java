package sec4;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;

public class Error {

	public static void main(String[] args) {
		
		@NonNull
		Observable<@NonNull Object> obsError = Observable.error(new Exception("Source Observable. Se jodio el programa"));
		
		obsError.subscribe(onNext->System.out.println(onNext),
				onError->System.out.println(onError),
				()->System.out.println("Completado mijo!!!"));
		
		Single.error(new Exception("Source Single. Se jodio el programa"))
		.subscribe(onSuccess->System.out.println("Printing Single:"+onSuccess),
				onError->System.out.println(onError));
		
		Maybe.error(new Exception("Source Maybe. Se jodio el programa"))
		.subscribe(onSuccess->System.out.println("Printing Maybe:"+onSuccess),
				onError->System.out.println(onError));
		
		Completable.error(new Exception("Source Completable. Se jodio el programa"))
		.subscribe(()->System.out.println("Printing Completable"),
				onError->System.out.println(onError));
		
		
		Observable<Object> obsWithError = Observable.just("Hola","a","todos",obsError,"pero","recupere :D");
		System.out.println("---Va a imprimir un Observable con un error---");
		
		obsWithError
		//.doOnComplete(()->System.out.println("Hice doOnComplete()"))
		.doOnError(e-> new Exception("Exception message").getCause() )
		//.doOnTerminate(()->System.out.println("Hice doOnTerminate()"))
		//.onErrorComplete()
		.onErrorReturn(e->"++++++++Me retorne por el error!!!+++++++")
		.subscribe(x->System.out.println(x));
		
	}

}
