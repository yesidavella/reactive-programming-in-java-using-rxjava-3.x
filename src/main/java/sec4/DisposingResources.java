package sec4;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;

public class DisposingResources {

	public static void main(String[] args) throws InterruptedException {
		
		@NonNull
		Observable<Long> source = Observable.interval(1, TimeUnit.SECONDS);
		Consumer<Long> consumer = e -> System.out.println("Consumer: "+e);
		Observer<Long> observer = new Observer<Long>() {

			@Override
			public void onSubscribe(@NonNull Disposable d) {
				System.out.println("Ejecutando onSubscribe() en obj. observer");
			}

			@Override
			public void onNext(Long l) {
				System.out.println("Ejecutando onNext() en obj. observer. Objeto:"+l);
			}

			@Override
			public void onError(@NonNull Throwable e) {
				System.out.println("Ejecutando onError() en obj. observer");				
			}

			@Override
			public void onComplete() {
				System.out.println("Ejecutando onComplete() en obj. observer");								
			}
		};
		
		@NonNull
		Disposable disposableConsumer = source.subscribe(consumer);
		source.subscribe(observer);
		
		Thread.sleep(10000);
		disposableConsumer.dispose();
		Thread.sleep(10000);
	}

}
