package sec4;

import io.reactivex.rxjava3.core.Observable;

public class OperatorsInAction {

	public static void main(String[] args) {
		
		Observable<Employee> obs = Observable.just(new Employee(101, "Alexa", 60000, 4.0),
				new Employee(123, "Dhwanit", 94000, 4.7), new Employee(236, "Mike", 65000, 4.0),
				new Employee(155, "Ella", 85000, 4.4), new Employee(443, "George", 50000, 3.6),
				new Employee(127, "Shreeja", 85000, 4.5), new Employee(509, "Daniel", 60000, 4.0),
				new Employee(344, "Lucy", 94000, 4.7), new Employee(509, "Harry", 75000, 4.3),
				new Employee(344, "Emma", 55000, 3.7));
		
		
		obs.filter(employee -> employee.rating >= 4)
		.subscribe(empl -> System.out.println("Employee rating >= 4:"+ empl));
		
		obs.sorted( (e1,e2)-> Double.compare(e2.getSalary(), e1.getSalary()))
		.take(4)
		.subscribe(empl->System.out.println("The 4 hights salaries are:"+empl));
		
		System.out.println("The accumulation of the 4 highest salaries are:");
		obs.sorted( (e1,e2)-> Double.compare(e2.getSalary(), e1.getSalary()))
		.take(4)
		.map(empl -> empl.getSalary())
		.reduce(912d,(s1,s2)->s1+s2)
		.subscribe(empl->System.out.println("The 4 hights salaries are:"+empl));
		
	}

}
