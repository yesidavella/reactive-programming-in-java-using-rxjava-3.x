package sec4;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.internal.schedulers.ScheduledDirectPeriodicTask;
import io.reactivex.rxjava3.observables.ConnectableObservable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ColdToHotObservable {

	public static void main(String[] args) throws InterruptedException {

		@NonNull
		Observable<Long> coldSource = Observable.intervalRange(0, 20, 0, 1, TimeUnit.SECONDS, Schedulers.computation());

		System.out.println("-------------COLD OBSERVABLES--(each subscriber begins from scratch)---------------");

		coldSource.subscribe(e -> System.out.println("Consumer1: " + e));

		Thread.sleep(10000);

		coldSource.subscribe(e -> System.out.println("Consumer2: " + e));

		Thread.sleep(10000);

		System.out.println("-----------HOT OBSERVABLES(multi-casting)-All observers get the same emission----------------");

		@NonNull
		ConnectableObservable<@NonNull Long> connObs = coldSource.publish();
		@NonNull
		Disposable connect = connObs.connect();
		connObs.subscribe(e -> System.out.println("Consumer3: " + e));
		Thread.sleep(10000);
		connObs.subscribe(e -> System.out.println("Consumer4: " + e));
		Thread.sleep(5000);
		connObs.subscribe(e -> System.out.println("Consumer5: " + e));
		Thread.sleep(5000);
	}

}
